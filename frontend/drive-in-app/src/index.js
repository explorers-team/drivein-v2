import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { AppBar, Avatar, BottomNavigation, BottomNavigationAction, Box, Button, createTheme, Divider, Grid, IconButton, List, ListItem, ListItemAvatar, ListItemText, ThemeProvider, Toolbar, Typography } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import authService from './services/auth.service';
import { lime } from '@mui/material/colors';

const root = ReactDOM.createRoot(document.getElementById('root'));

const handleLogoutClick = (event) => {
  console.info("Logout clicked!");
  authService.logout();
  window.location.reload();
};

const darkTheme = createTheme({
  palette: {
    primary: {
      main: lime[500],
    },
  },
});

root.render(
  <React.StrictMode>
    <ThemeProvider theme={darkTheme}>
      <Box sx={{ flexGrow: 1 }} visibility={false}>
        <AppBar position="fixed" >
          <Toolbar>
            {authService.getCurrentUser() && <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>}
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              DriveIn
            </Typography>
            {authService.getCurrentUser() && <Button color="inherit" href='/signin' onClick={handleLogoutClick}>Logout</Button>}
          </Toolbar>
        </AppBar>

        <App />

      </Box>
    </ThemeProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
