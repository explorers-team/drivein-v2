import axios from "axios";

const API_URL = 'http://localhost:5263/api/';

class AuthService {
    
    async login(login, password) {
        return axios
            .post(API_URL + "v1/auth/login", {
                login,
                password
            })
            .then(response => {
                if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }

                return response.data;
            });
    }

    logout() {
        localStorage.removeItem("user");
    }

    register(username, email, password) {
        return axios.post(API_URL + "signup", {
            username,
            email,
            password
        });
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem('user'));
    }

    getCurrentUserName() {
        return JSON.parse(localStorage.getItem('user')).userName;
    }
}

export default new AuthService();