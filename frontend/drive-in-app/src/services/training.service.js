import axios from 'axios';
import getAuthHeader from './auth-header';

const API_URL = 'http://localhost:5263/api/';

class TrainingService {

    async getTrainings(){

        var authHeader = getAuthHeader();

        return axios.get(API_URL + 'v1/trainings', {  headers: authHeader }); 
    }

    async getTraining(id){

        var authHeader = getAuthHeader();

        return axios.get(API_URL + `v1/trainings/${id}`, {  headers: authHeader }); 
    }

    // Create training.
    async createTraining(dateBegin, dateEnd, distance, comment, trainingPoints){

        var authHeader = getAuthHeader();

        var trainingPoints = trainingPoints.map((item) =>  
            ({
                latitude: item[0],
                longitude: item[1]
            })
        );
    
        return axios.post(API_URL + 'v1/trainings', 
        {
            dateBegin,
            dateEnd,
            distance,
            comment,
            trainingPoints
        },
        {  headers: authHeader }); 
    }
}

export default TrainingService;