export default function getAuthHeader() {
    var user = JSON.parse(localStorage.getItem('user'));
  
    if(!user || !user.accessToken) // todo - убрать (только для отладки)
        user = {
          accessToken : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6IkpXVCJ9.eyJ1c2VySWQiOiJhOGRlYjUzOC00NGU1LTQ5NjItODc4My0xNWE0MTRkMjNmMDIiLCJwaG9uZU51bWJlciI6IjcwMDAwMDAwMDAwIiwiZXhwIjoxNjY4MzQ3NTg4fQ._gzwnc0oH5HUD-wDDlMOtfU6yOM_XJGm8erczZH3Pzs" // TODO - для отладки
        };

    if (user && user.accessToken) {
      return { Authorization: 'Bearer ' + user.accessToken };
    } else {
      return {};
    }
  }