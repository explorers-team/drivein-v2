import { Box, Button, Container, Grid, Typography } from '@mui/material';
import React from 'react';

function NotFoundPage() {
    return <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          minHeight: '100vh'
        }}
      >
        <Container maxWidth="md">
          <Grid container >
            <Grid xs={8}>
              <Typography variant="h1">
                404
              </Typography>
              <Typography variant="h6">
                The page you’re looking for doesn’t exist.
              </Typography>
              <Button variant="contained" href='/'>Back Home</Button>
            </Grid>
          </Grid>
        </Container>
      </Box>     
}

export default NotFoundPage;