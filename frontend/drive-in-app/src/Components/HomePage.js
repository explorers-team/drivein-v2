import RouteOutlinedIcon from '@mui/icons-material/RouteOutlined';
import ChatIcon from '@mui/icons-material/Chat';
import Trainings from './Trainings';
import { BottomNavigation, BottomNavigationAction, Container, Grid, Stack } from '@mui/material';
import { useState } from 'react';
import ChatV1 from './Chat/V1/ChatV1';
import ChatV2 from './Chat/V2/ChatV2';
import ChatDemo from './Chat/ChatDemo';

export default function HomePage() {
    const TRAINING_TAB = "Trainings";
    const CHAT_TAB = "Chat";
    const [selectedTab, setSelectedTab] = useState(0);

    return (
        <Grid>

            <Container sx={{ marginTop: 8, marginBottom: 8 }}>
                {selectedTab == 0 && <Trainings />}
                {selectedTab == 1 && <ChatV2 />}
                {/* {selectedTab == 1 && <ChatDemo />} */}
                {/* {selectedTab == 1 && <ChatV1 />} */}
                {/* {selectedTab == 1 && <ChatV2 />} */}
            </Container>

            <BottomNavigation
                sx={{ position: 'fixed', bottom: 0, width: 1.0 }}
                value={selectedTab}
                onChange={(event, newValue) => {
                    setSelectedTab(newValue);
                }}
            >
                <BottomNavigationAction label={TRAINING_TAB} icon={<RouteOutlinedIcon />} />
                <BottomNavigationAction label={CHAT_TAB} icon={<ChatIcon />} />
            </BottomNavigation>
        </Grid>
    );
}