import { Grid, ListItem, ListItemText } from '@mui/material';
import React from 'react';

const Message = (props) => (
    <ListItem>
        <Grid container>
            <Grid item xs={12}>
                <ListItemText align="right" primary={props.message}></ListItemText>
            </Grid>
            <Grid item xs={12}>
                <ListItemText align="right" secondary="09:30"></ListItemText>
            </Grid>
        </Grid>
    </ListItem>
);

export default Message;