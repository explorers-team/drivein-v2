import { Box, Fab, Grid, TextField } from '@mui/material';
import React, { useState } from 'react';
import SendIcon from '@mui/icons-material/Send';
import authService from '../../../services/auth.service';

const ChatInput = (props) => {
    const [user, setUser] = useState('Петр');
    const [message, setMessage] = useState();
    const onMessageChange = (e) => setMessage(e.target.value);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const isUserProvided = user && user !== '';
        const isMessageProvided = message && message !== '';
        const userName = authService.getCurrentUserName();

        if (isUserProvided && isMessageProvided) {
            props.sendMessage(userName, message);
        }
        else {
            alert('Please insert an user and a message.');
        }
    }

    return (
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <Grid container style={{ padding: '20px' }}
                sx={{
                    position: 'fixed',
                    bottom: (theme) => theme.spacing(6),
                    right: (theme) => theme.spacing(2)
                }}>
                <Grid item xs={11}>
                    <TextField id="outlined-basic-email" label="Type Something" value={message} onChange={onMessageChange} fullWidth />
                </Grid>
                <Grid item xs={1} align="right">
                    <Fab color="primary" aria-label="add" type="submit"><SendIcon /></Fab>
                </Grid>
            </Grid>
        </Box>
    )
};

export default ChatInput;