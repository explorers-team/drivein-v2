import { List } from '@mui/material';
import React from 'react';
import Message from '../V1/Message';

const ChatWindow = (props) => {
    const chat = props.chat
        .map(m => <Message 
            key={Date.now() * Math.random()}
            user={m.user}
            message={m.message}/>);

    return(
        <List>
            {chat}
        </List>
    )
};

export default ChatWindow;