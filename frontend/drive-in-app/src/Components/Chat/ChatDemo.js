import React from 'react';
import SendIcon from '@mui/icons-material/Send';
import { List, ListItem, ListItemIcon, ListItemText, Divider, Avatar, TextField, Typography, Box, Fab, Paper, useStyles, Grid, makeStyles } from '@mui/material';


const ChatDemo = () => {

    return (
        <div>
            <Grid container style={{ width: '100%', height: '100vh' }}>
                <Grid item xs={12}>
                    <List >
                        <ListItem key="1">
                            <Grid container>
                                <Grid item xs={12}>
                                    <ListItemText align="right" primary="Hey man, What's up ?"></ListItemText>
                                </Grid>
                                <Grid item xs={12}>
                                    <ListItemText align="right" secondary="09:30"></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>
                        <ListItem key="2">
                            <Grid container>
                                <Grid item xs={12}>
                                    <ListItemText align="left" primary="Hey, Iam Good! What about you ?"></ListItemText>
                                </Grid>
                                <Grid item xs={12}>
                                    <ListItemText align="left" secondary="09:31"></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>
                        <ListItem key="3">
                            <Grid container>
                                <Grid item xs={12}>
                                    <ListItemText align="right" primary="Cool. i am good, let's catch up!"></ListItemText>
                                </Grid>
                                <Grid item xs={12}>
                                    <ListItemText align="right" secondary="10:30"></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>
                    </List>
                    <Divider />
                    <Grid container style={{ padding: '20px' }}
                        sx={{
                            position: 'fixed',
                            bottom: (theme) => theme.spacing(6),
                            right: (theme) => theme.spacing(2)
                        }}>
                        <Grid item xs={11}>
                            <TextField id="outlined-basic-email" label="Type Something" fullWidth />
                        </Grid>
                        <Grid item xs={1} align="right">
                            <Fab color="primary" aria-label="add"><SendIcon /></Fab>
                        </Grid>
                    </Grid>

                </Grid>
            </Grid>
        </div>
    );
}

export default ChatDemo;