import { useEffect, useState } from "react";
import { Avatar, Fab, List, ListItem, ListItemAvatar, ListItemText, Stack} from '@mui/material';
import TrainingService from "../services/training.service";
import { useHistory } from 'react-router-dom';
import AddIcon from '@mui/icons-material/Add';
import RouteOutlinedIcon from '@mui/icons-material/RouteOutlined';
export default function Trainings() {

    const [loading, setLoading] = useState(true);
    const [trainings, setTrainings] = useState();

    const history = useHistory();

    const loadTrainings = async () => {
        setLoading(true);
        try {
            const { data: response } = await new TrainingService().getTrainings();
            setTrainings(response);
        } catch (error) {
            console.error(error.message);
        }
        setLoading(false);
    };

    useEffect(() => {
        loadTrainings();
    }, []);

    const handleItemClick = async (event, trainingId) => {
        console.info("Item clicked " + trainingId);
        history.push(`/trainings/${trainingId}`);
    }

    const handleNewTrainingClick = async (event) => {
        history.push('/training/new');
    }

    return (
        <Stack>
            {loading && <div>Loading...</div>}
            {!loading && (
                <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                    {trainings &&
                        trainings.map((training) => (
                            <ListItem key={training.id} onClick={(event) => handleItemClick(event, training.id)}>
                                <ListItemAvatar>
                                    <Avatar>
                                        <RouteOutlinedIcon />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={training.distance + ' km (' + training.dateEnd + ')'} secondary={training.comment} />
                            </ListItem>
                        ))}
                </List>)}

            <Fab color="primary" aria-label="add"
                sx={{
                    position: 'fixed',
                    bottom: (theme) => theme.spacing(5),
                    right: (theme) => theme.spacing(2)
                }}
                onClick={handleNewTrainingClick}
            >
                <AddIcon />
            </Fab>
        </Stack>
    );
}