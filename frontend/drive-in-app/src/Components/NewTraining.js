import { Button, Card, Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Fab, Grid, IconButton, InputAdornment, Stack, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { MapContainer, Marker, Polyline, Popup, TileLayer } from "react-leaflet";
import { useParams } from "react-router-dom";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import StopIcon from '@mui/icons-material/Stop';
import GeoService from "../services/geo.service";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import TrainingService from "../services/training.service";
import AntiPolicyCurtains from "./AntiPolicyCurtains";

export default function NewTraining() {
    const geoService = new GeoService();
    const trainingService = new TrainingService();

    const [currentPosition, setCurrentPosition] = useState([45.0276784, 39.0183306]);
    const [currentTrainingPoints, setCurrentTrainingPoints] = useState([[45.0276784, 39.0183306]
    ]);

    const [startTrainingDate, setStartTrainingDate] = useState(new Date().toISOString());
    const [finishTrainingDate, setFinishTrainingDate] = useState(new Date().toISOString());

    const [time, setTime] = useState(0);
    const [running, setRunning] = useState(false);

    const [openStopTrainingDialog, setOpenStopTrainingDialog] = useState(false);
    const [openSaveSuccessModal, setOpenSaveSuccessModal] = useState(false);

    useEffect(() => {
        let interval;
        if (running) {
            interval = setInterval(() => {
                setTime((prevTime) => prevTime + 10);
                var newPosition = geoService.GetCurrentPosition();
                setCurrentTrainingPoints(currentTrainingPoints => [...currentTrainingPoints, newPosition]);
                setCurrentPosition(newPosition);
            }, 10);
        } else if (!running) {
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [running]);

    const handleStartTrainingClick = () => {
        
        setRunning(true);
        setStartTrainingDate(new Date().toISOString())
    }

    const handleStopTrainingClick = () => {
        pauseTraining();
        setOpenStopTrainingDialog(true);
    };

    const handleStopTrainingModalClose = (isStopTraining) => {

        if (isStopTraining) {
            stopTraining();
        } else {
            continueTraining();
        }

        setOpenStopTrainingDialog(false);
    };

    // Set training on pause
    const pauseTraining = () => {
        setRunning(false);
    }

    const continueTraining = () => {
        setRunning(true);
    }

    const stopTraining = () => {
        setFinishTrainingDate(new Date().toISOString());
        saveCurrentTrainingResult();
    }

    // Save training result to server 
    const saveCurrentTrainingResult = () => {
        
        var distance = geoService.GetDistance();

        trainingService.createTraining(
            startTrainingDate, 
            finishTrainingDate,
             distance,
              "Новая тренировка " + startTrainingDate,
              currentTrainingPoints);
        setOpenSaveSuccessModal(true);
    }

    return (
        <div>
            <MapContainer
                center={currentPosition}
                zoom={14}
                zoomControl={false}
                scrollWheelZoom={true}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={currentPosition}>
                    <Popup>
                        You are HERE now. Let's go!
                        Press start to start your training!
                    </Popup>
                </Marker>
                <Polyline positions={currentTrainingPoints} color={'red'} />
                <AntiPolicyCurtains />
            </MapContainer>

            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                justifyContent="center"
            >
                <Grid>
                    <Typography variant="h2" >
                        {("0" + Math.floor((time / 3600000) % 3600)).slice(-2)}:
                        {("0" + Math.floor((time / 60000) % 60)).slice(-2)}:
                        {("0" + Math.floor((time / 1000) % 60)).slice(-2)}.
                        {("0" + ((time / 10) % 100)).slice(-2)}
                    </Typography>
                </Grid>

                <Grid item xs={3} >
                    <Container>
                        {!running && <Fab color="primary" sx={{ m: 1 }} onClick={handleStartTrainingClick}>
                            <PlayArrowIcon />
                        </Fab>}
                        {running && <Fab color="primary" sx={{ m: 1 }} onClick={() => setRunning(false)}>
                            <PauseIcon />
                        </Fab>}
                        <Fab sx={{ m: 1 }} onClick={() => handleStopTrainingClick()}>
                            <StopIcon />
                        </Fab>
                    </Container>
                </Grid>

            </Grid>

            {/* Dialog to stop training  */}
            <Dialog
                open={openStopTrainingDialog}
                onClose={handleStopTrainingModalClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Have you arrived?"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Are you sure you want to end current training? Let's save your results.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => handleStopTrainingModalClose(false)}>No</Button>
                    <Button onClick={() => handleStopTrainingModalClose(true)} autoFocus>
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>

            {/* Succuss training saving modal  */}
            <Dialog
                open={openSaveSuccessModal}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Urah!"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <CheckCircleOutlineIcon color="success" />
                        We are saved your results!
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenSaveSuccessModal(false)}>OK</Button>
                </DialogActions>
            </Dialog>

        </div>
    );
}