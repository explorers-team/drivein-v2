import { Container } from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from 'react';

// Curtains anti policy
export default function AntiPolicyCurtains(props) {

    return (
            <Box
                sx={{
                    position: 'absolute',
                    width: 18,
                    height: 16,
                    bottom: 0,
                    right: props.right ?? 197,
                    zIndex: 16777271,
                    backgroundColor: 'white'
                }}
            />)
}