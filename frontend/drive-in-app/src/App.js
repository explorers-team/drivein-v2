import './App.css';
import * as React from 'react';
import Button from '@mui/material/Button';
import SignIn from './components/SignIn';
import { appBarClasses, Container, Stack } from '@mui/material';
import { Redirect, Route, Switch, BrowserRouter } from 'react-router-dom';
import HomePage from './components/HomePage';
import NotFoundPage from './components/NotFoundPage';
import SignUp from './components/SignUp';
import TrainingDetail from './components/TrainingDetails';
import NewTraining from './components/NewTraining';

function MyApp() {
  return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={SignIn} />
          <Route exact path='/signin' component={SignIn} />
          <Route exact path='/signup' component={SignUp} />
          <Route exact path='/home' component={HomePage} /> 
          <Route exact path='/training/new' component={NewTraining}/>
          <Route exact path='/trainings/:trainingId' component={TrainingDetail} />
          <Route path="/404" component={NotFoundPage} />
          <Route path="*" component={NotFoundPage} />
        </Switch>
      </BrowserRouter>
  );
}

export default MyApp;